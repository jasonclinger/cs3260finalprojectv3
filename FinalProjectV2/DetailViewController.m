//
//  DetailViewController.m
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/31/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "DetailViewController.h"
#import "GraphviewController.h"
#import "TFHpple.h"
#import "Tutorial.h"
#import "Contributor.h"

@interface DetailViewController (){
    NSMutableArray *_objectsMetals;
    NSMutableArray *_objectsSpot;
  
}

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    array = [NSArray arrayWithObjects: @"Past 24 Hours", @"Past 7 Days", @"Past Month", @"Past 3 Months", @"Past 6 Months", @"Past Year", nil];
    
    _myPickerView.delegate = self;
    _myPickerView.dataSource = self;
    _myPickerView.showsSelectionIndicator = YES;
    [self.view addSubview:_myPickerView];
    [self loadTutorialsSpot];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadTutorialsMetals];
    });
    
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
    NSManagedObject* FirstEntity;
    NSManagedObject* SecondEntity;
    NSManagedObject* ThirdEntity;
    NSManagedObject* FourthEntity;
    
    if ([preName isEqualToString:@"Gold"]) {
        FirstEntity = [NSEntityDescription insertNewObjectForEntityForName:@"FirstEntity" inManagedObjectContext:ad.managedObjectContext];
    }
    
    if ([preName isEqualToString:@"Silver"]) {
        SecondEntity = [NSEntityDescription insertNewObjectForEntityForName:@"SecondEntity" inManagedObjectContext:ad.managedObjectContext];
    }
    if ([preName isEqualToString:@"Platinum"]) {
        ThirdEntity = [NSEntityDescription insertNewObjectForEntityForName:@"ThirdEntity" inManagedObjectContext:ad.managedObjectContext];
    }
    
    if ([preName isEqualToString:@"Palladium"]) {
        FourthEntity = [NSEntityDescription insertNewObjectForEntityForName:@"FourthEntity" inManagedObjectContext:ad.managedObjectContext];
    }
    
    Tutorial *thisTutorialSpot = [_objectsSpot objectAtIndex:1];
    _spotBuy.text = thisTutorialSpot.title;
    
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-21600];
    
    
    if ([preName isEqualToString:@"Gold"]) {
        [FirstEntity setValue:thisTutorialSpot.title forKey:@"spotGoldEntity"];
        [FirstEntity setValue:date forKey:@"spotGoldTimeEntity"];
    }
    
    if ([preName isEqualToString:@"Silver"]) {
        [SecondEntity setValue:thisTutorialSpot.title forKey:@"spotSilverEntity"];
        [SecondEntity setValue:date forKey:@"spotSilverTimeEntity"];
    }
    
    if ([preName isEqualToString:@"Platinum"]) {
        [ThirdEntity setValue:thisTutorialSpot.title forKey:@"spotPlatinumEntity"];
        [ThirdEntity setValue:date forKey:@"spotPlatinumTimeEntity"];
    }
    
    if ([preName isEqualToString:@"Palladium"]) {
        [FourthEntity setValue:thisTutorialSpot.title forKey:@"spotPalladiumEntity"];
        [FourthEntity setValue:date forKey:@"spotPalladiumTimeEntity"];
    }
    
    
    NSError* error;
    if (![ad.managedObjectContext save:&error]) {
        NSLog(@"Save failed");
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    _metalName.text = preName;
    _roundName.text = preRoundName;
    _eagleName.text = preEagleName;
    _mapleName.text = preMapleName;
    _junkName.text = preJunkName;
    
    Tutorial *thisTutorialSpot = [_objectsSpot objectAtIndex:1];
    _spotBuy.text = thisTutorialSpot.title;
    
    if ([preName isEqualToString:@"Gold"]) {
        Tutorial *thisTutorialEagleBuy = [_objectsMetals objectAtIndex:0];
        Tutorial *thisTutorialEagleSell = [_objectsMetals objectAtIndex:1];
        
        Tutorial *thisTutorialRoundBuy = [_objectsMetals objectAtIndex:2];
        Tutorial *thisTutorialRoundSell = [_objectsMetals objectAtIndex:3];
        
        Tutorial *thisTutorialJunkBuy = [_objectsMetals objectAtIndex:4];
        Tutorial *thisTutorialJunkSell = [_objectsMetals objectAtIndex:5];
        
        Tutorial *thisTutorialMapleBuy = [_objectsMetals objectAtIndex:6];
        Tutorial *thisTutorialMapleSell = [_objectsMetals objectAtIndex:7];
        
        _roundBuy.text = thisTutorialRoundBuy.title;
        _roundSell.text = thisTutorialRoundSell.title;
        
        _eagleBuy.text = thisTutorialEagleBuy.title;
        _eagleSell.text = thisTutorialEagleSell.title;
        
        _mapleBuy.text = thisTutorialMapleBuy.title;
        _mapleSell.text = thisTutorialMapleSell.title;
        
        _junkBuy.text = thisTutorialJunkBuy.title;
        _junkSell.text = thisTutorialJunkSell.title;
        
    }
    else if ([preName isEqualToString:@"Silver"]){
        Tutorial *thisTutorialEagleBuy = [_objectsMetals objectAtIndex:4];
        Tutorial *thisTutorialEagleSell = [_objectsMetals objectAtIndex:5];
        
        Tutorial *thisTutorialRoundBuy = [_objectsMetals objectAtIndex:2];
        Tutorial *thisTutorialRoundSell = [_objectsMetals objectAtIndex:3];
        
        Tutorial *thisTutorialJunkBuy = [_objectsMetals objectAtIndex:0];
        Tutorial *thisTutorialJunkSell = [_objectsMetals objectAtIndex:1];
        
        Tutorial *thisTutorialMapleBuy = [_objectsMetals objectAtIndex:6];
        Tutorial *thisTutorialMapleSell = [_objectsMetals objectAtIndex:7];
        
        _roundBuy.text = thisTutorialRoundBuy.title;
        _roundSell.text = thisTutorialRoundSell.title;
        
        _eagleBuy.text = thisTutorialEagleBuy.title;
        _eagleSell.text = thisTutorialEagleSell.title;
        
        _mapleBuy.text = thisTutorialMapleBuy.title;
        _mapleSell.text = thisTutorialMapleSell.title;
        
        _junkBuy.text = thisTutorialJunkBuy.title;
        _junkSell.text = thisTutorialJunkSell.title;
        
    }
    else if ([preName isEqualToString:@"Platinum"]){
        Tutorial *thisTutorialMapleBuy = [_objectsMetals objectAtIndex:0];
        Tutorial *thisTutorialMapleSell = [_objectsMetals objectAtIndex:1];
        
        _roundBuy.text = preRoundBuy;
        _roundSell.text = preRoundSell;
        
        _eagleBuy.text = preEagleBuy;
        _eagleSell.text = preEagleSell;
        
        _mapleBuy.text = thisTutorialMapleBuy.title;
        _mapleSell.text = thisTutorialMapleSell.title;
        
        _junkBuy.text = preJunkBuy;
        _junkSell.text = preJunkSell;

    }
    else{ //Palladium
        Tutorial *thisTutorialEagleBuy = [_objectsMetals objectAtIndex:0];
        Tutorial *thisTutorialEagleSell = [_objectsMetals objectAtIndex:1];
        
        Tutorial *thisTutorialMapleBuy = [_objectsMetals objectAtIndex:2];
        Tutorial *thisTutorialMapleSell = [_objectsMetals objectAtIndex:3];
        
        _roundBuy.text = preRoundBuy;
        _roundSell.text = preRoundSell;
        
        _eagleBuy.text = thisTutorialEagleBuy.title;
        _eagleSell.text = thisTutorialEagleSell.title;
        
        _mapleBuy.text = thisTutorialMapleBuy.title;
        _mapleSell.text = thisTutorialMapleSell.title;

        _junkBuy.text = preJunkBuy;
        _junkSell.text = preJunkSell;
    }
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return array.count;
}


- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    switch(row) {
        case 0:
            title = array[0];
            break;
        case 1:
            title = array[1];
            break;
        case 2:
            title = array[2];
            break;
        case 3:
            title = array[3];
            break;
        case 4:
            title = array[4];
            break;
        case 5:
            title = array[5];
            break;

    }
    return title;
    
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    //Here, like the table view you can get the each section of each row if you've multiple sections
    NSLog(@"Selected Timeframe: %@. Index of selected time: %li", [array objectAtIndex:row], (long)row);
    
    switch(row) {
        case 0:
            pickerTimeFrame = @"24";
            break;
        case 1:
            pickerTimeFrame = @"days";
            break;
        case 2:
            pickerTimeFrame = @"oneMonth";
            break;
        case 3:
            pickerTimeFrame = @"threeMonths";
            break;
        case 4:
            pickerTimeFrame = @"sixMonths";
            break;
        case 5:
            pickerTimeFrame = @"year";
            break;
    }
    
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier  isEqual: @"toGraphView"]) {
        
        UIButton *button = sender;
        GraphViewController *graphView = segue.destinationViewController;
        graphView->preGraphName = preName;
        
        if ([pickerTimeFrame isEqualToString:@"days"]) {
            NSLog(@"I picked the 7 day graph!");
            chosenDateRange = @"days";
        }
        else if ([pickerTimeFrame isEqualToString:@"oneMonth"]) {
            NSLog(@"I picked the 1 month graph!");
            chosenDateRange = @"oneMonth";
        }
        else if ([pickerTimeFrame isEqualToString:@"threeMonths"]) {
           NSLog(@"I picked the 3 month graph!");
            chosenDateRange = @"threeMonths";
        }
        else if ([pickerTimeFrame isEqualToString:@"sixMonths"]) {
           NSLog(@"I picked the 6 month graph!");
            chosenDateRange = @"sixMonths";
        }
        else if ([pickerTimeFrame isEqualToString:@"year"]) {
            NSLog(@"I picked the year graph!");
            chosenDateRange = @"year";
        }
        else { //equals "24", also default value
            NSLog(@"I picked the 24 graph!");
            chosenDateRange = @"24";
        }
        
        graphView->preDateRange = chosenDateRange;
        graphView->preCurrentSpotPrice = _spotBuy.text;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loadTutorialsMetals {
    
    NSURL *tutorialsUrl = [NSURL URLWithString:@"http://www.rustcoin.com/metal_prices.php"];
    NSData *tutorialsHtmlData = [NSData dataWithContentsOfURL:tutorialsUrl];
    
    TFHpple *tutorialsParser = [TFHpple hppleWithHTMLData:tutorialsHtmlData];
    
    NSString *tutorialsXpathQueryString;
    
    if ([preName isEqualToString:@"Gold"]) {
        tutorialsXpathQueryString = @"//div[@class='featured-spot-products']/div[1]/table";
    }
    else if ([preName isEqualToString:@"Silver"]){
        tutorialsXpathQueryString = @"//div[@class='featured-spot-products']/div[2]/table";
    }
    else if ([preName isEqualToString:@"Platinum"]){
        tutorialsXpathQueryString = @"//div[@class='featured-spot-products']/div[3]/table";
    }
    else{ //Palladium
        tutorialsXpathQueryString = @"//div[@class='featured-spot-products']/div[4]/table";
    }
    
    NSArray *tutorialsNodes = [tutorialsParser searchWithXPathQuery:tutorialsXpathQueryString];
    
    NSMutableArray *newTutorials = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (TFHppleElement *element in tutorialsNodes) {
        
        for (TFHppleElement *child in element.children) {
            if ([child.tagName isEqualToString:@"tr"]) {
                
                for(TFHppleElement *grandChild in child.children)
                    if ([grandChild.tagName isEqualToString:@"td"]) {
                        
                        for(TFHppleElement *greatGrandChild in grandChild.children)
                            if ([greatGrandChild.tagName isEqualToString:@"span"]) {
                                @try {
                                    
                                    Tutorial *tutorial = [[Tutorial alloc] init];
                                    [newTutorials addObject:tutorial];
                                    
                                    
                                    tutorial.title = [[greatGrandChild firstChild] content];
                                    //tutorial.url = [greatGrandChild objectForKey:@"nodeContent"];
                                    NSLog(@"found a price");
                                    
                                }
                                @catch (NSException *e) {}
                                
                            }
                        
                    }
                
            } else if ([child.tagName isEqualToString:@"class"]) {
                
            }
        }
    }
    
    _objectsMetals = newTutorials;
    //[self reloadData];
}

-(void)loadTutorialsSpot {
    
    NSURL *tutorialsUrl = [NSURL URLWithString:@"http://www.rustcoin.com/metal_prices.php"];
    NSData *tutorialsHtmlData = [NSData dataWithContentsOfURL:tutorialsUrl];
    
    TFHpple *tutorialsParser = [TFHpple hppleWithHTMLData:tutorialsHtmlData];
    
    NSString *tutorialsXpathQueryString;
    
    if ([preName isEqualToString:@"Gold"]) {
        tutorialsXpathQueryString = @"//div[@class='charts']/div[1]/div[1]";
    }
    else if ([preName isEqualToString:@"Silver"]){
        tutorialsXpathQueryString = @"//div[@class='charts']/div[2]/div[1]";
    }
    else if ([preName isEqualToString:@"Platinum"]){
        tutorialsXpathQueryString = @"//div[@class='charts']/div[3]/div[1]";
    }
    else{ //Palladium
        tutorialsXpathQueryString = @"//div[@class='charts']/div[4]/div[1]";
    }
    
    NSArray *tutorialsNodes = [tutorialsParser searchWithXPathQuery:tutorialsXpathQueryString];
    
    NSMutableArray *newTutorials = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (TFHppleElement *element in tutorialsNodes) {
        
        for (TFHppleElement *child in element.children) {
            if ([child.tagName isEqualToString:@"table"]) {
                
                for(TFHppleElement *grandChild in child.children)
                    if ([grandChild.tagName isEqualToString:@"tr"]) {
                        
                        for(TFHppleElement *greatGrandChild in grandChild.children)
                            if ([greatGrandChild.tagName isEqualToString:@"td"]) {
                                @try {
                                    
                                    Tutorial *tutorial = [[Tutorial alloc] init];
                                    [newTutorials addObject:tutorial];
                                    
                                    
                                    tutorial.title = [[greatGrandChild firstChild] content];
                                    //tutorial.url = [greatGrandChild objectForKey:@"nodeContent"];
                                    NSLog(@"found a price");
                                    
                                }
                                @catch (NSException *e) {}
                                
                            }
                        
                    }
                
            } else if ([child.tagName isEqualToString:@"class"]) {
                
            }
        }
    }
    
    _objectsSpot = newTutorials;
    //[self.collectionView reloadData];
}


@end
