//
//  CollectionViewController.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/29/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FirstEntity.h"
#import "SecondEntity.h"
#import "ThirdEntity.h"
#import "FourthEntity.h"

@interface CollectionViewController : UICollectionViewController
{
    NSArray* array;
    NSMutableArray* arrayOfMetals;
    NSData* data;
    UICollectionView* cView;
    id d;
}

@end
