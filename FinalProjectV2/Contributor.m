//
//  Contributor.m
//  FinalProjectV2
//
//  Created by Jason Clinger on 4/2/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "Contributor.h"

@implementation Contributor

@synthesize name = _name;
@synthesize imageUrl = _imageUrl;

@end
