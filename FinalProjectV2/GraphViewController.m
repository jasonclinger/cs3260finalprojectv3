//
//  GraphViewController.m
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/31/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "GraphViewController.h"
#import <UIKit/UIKit.h>
#import <JBChartView.h>
#import <JBBarChartView.h>
#import <JBLineChartView.h>
#import <JBLineChartLine.h>
#import <JBLineChartPoint.h>
#import <JBLineChartDotsView.h>
#import <JBLineChartDotView.h>
#import <JBLineChartLinesView.h>
#import "CollectionViewCell.h"
#import "DetailViewController.h"


@interface GraphViewController (){
    //NSMutableArray *_objectsSpot;
    FirstEntity* fe;
    FirstEntity* feTest;
    SecondEntity* se;
    SecondEntity* seTest;
    ThirdEntity* te;
    ThirdEntity* teTest;
    FourthEntity* fte;
    FourthEntity* fteTest;
}

@end

@implementation GraphViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self reloadData];
    

    chartData1 = [[NSMutableArray alloc]init];
    chartData2 = [[NSMutableArray alloc]init];
    chartData3 = [[NSMutableArray alloc]init];
    chartData4 = [[NSMutableArray alloc]init];

    JBBarChartView *barChartView = [[JBBarChartView alloc] init];
    barChartView.dataSource = self;
    barChartView.delegate = self;
    barChartView.backgroundColor = [UIColor lightGrayColor];
    barChartView.layer.borderWidth =1;
    
    [self.view addSubview:barChartView];
    
    barChartView.frame = CGRectMake(64, 150, 300, 300);
    
    [barChartView reloadData];
    [self loadHighLowAmounts];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    _graphMetalTitle.text = preGraphName;
    _currentSpotPriceGraph.text = preCurrentSpotPrice;
    
    if ([preDateRange isEqualToString:@"oneMonth"]) {
        
         _timePeriod.text = @"Spot Prices over the past month";
    }
    else if ([preDateRange isEqualToString:@"threeMonths"]) {
        
        _timePeriod.text = @"Spot Prices over the past 3 months";
    }
    else if ([preDateRange isEqualToString:@"sixMonths"]) {
        
        _timePeriod.text = @"Spot Prices over the past 6 months";
    }
    else if ([preDateRange isEqualToString:@"year"]) {
        
        _timePeriod.text = @"Spot Prices over the past year";
    }
    else if ([preDateRange isEqualToString:@"days"]) {
        
        _timePeriod.text = @"Spot Prices over the past 7 days";
    }
    else { //preDateRange equals "24", also default value

        _timePeriod.text = @"Spot Prices over the past 24 hours";
    }
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadData];
}

-(void) reloadData{
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSError* error;
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription* entity;
    NSEntityDescription* entity2;
    NSEntityDescription* entity3;
    NSEntityDescription* entity4;
    

    
    if ([preGraphName isEqualToString:@"Gold"]) {
        NSEntityDescription* entity = [NSEntityDescription entityForName:@"FirstEntity" inManagedObjectContext:ad.managedObjectContext];
        [request setEntity:entity];
    }
    if ([preGraphName isEqualToString:@"Silver"]) {
        NSEntityDescription* entity2 = [NSEntityDescription entityForName:@"SecondEntity" inManagedObjectContext:ad.managedObjectContext];
        [request setEntity:entity2];
    }
    if ([preGraphName isEqualToString:@"Platinum"]) {
        NSEntityDescription* entity3 = [NSEntityDescription entityForName:@"ThirdEntity" inManagedObjectContext:ad.managedObjectContext];
        [request setEntity:entity3];
    }
    if ([preGraphName isEqualToString:@"Palladium"]) {
        NSEntityDescription* entity4 = [NSEntityDescription entityForName:@"FourthEntity" inManagedObjectContext:ad.managedObjectContext];
        [request setEntity:entity4];
    }
    
    array = [ad.managedObjectContext executeFetchRequest:request error:&error];
    NSLog(@"Graph-CData");
    
    //////////////////////////////////////////////////////
    //1 day = -86400, - 21600 = -108,000 seconds
    
    if ([preGraphName isEqualToString:@"Gold"]) {
        
        if ([preDateRange isEqualToString:@"oneMonth"]) {
            //1 day = -2592000, - 21600 = -2613600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-2613600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                feTest = array[i];
                testdate9 = feTest.spotGoldTimeEntity;
                
                // if "date" is earlier than testdate9 then proceed with this block, meaning its withing date range
                // Otherwise skip this block
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"threeMonths"]) {
            //1 day = -7776000, - 21600 = -7797600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-7797600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                feTest = array[i];
                testdate9 = feTest.spotGoldTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"sixMonths"]) {
            //1 day = -15552000, - 21600 = -15573600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-15573600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                feTest = array[i];
                testdate9 = feTest.spotGoldTimeEntity;

                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"year"]) {
            //1 day = -31104000, - 21600 = -31125600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-31125600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                feTest = array[i];
                testdate9 = feTest.spotGoldTimeEntity;

                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"days"]) {
            //1 day = -86400, - 21600 = -108,000 seconds; 7 days = 626400
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-626400];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                feTest = array[i];
                testdate9 = feTest.spotGoldTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else { //preDateRange equals "24", also default value
            //1 day = -86400, - 21600 = -108,000 seconds; 7 days = 626400
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-108000];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                feTest = array[i];
                testdate9 = feTest.spotGoldTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
        }

    }
    if ([preGraphName isEqualToString:@"Silver"]) {
        if ([preDateRange isEqualToString:@"oneMonth"]) {
            //1 day = -2592000, - 21600 = -2613600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-2613600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                seTest = array[i];
                testdate9 = seTest.spotSilverTimeEntity;

                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"threeMonths"]) {
            //1 day = -7776000, - 21600 = -7797600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-7797600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                seTest = array[i];
                testdate9 = seTest.spotSilverTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"sixMonths"]) {
            //1 day = -15552000, - 21600 = -15573600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-15573600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                seTest = array[i];
                testdate9 = seTest.spotSilverTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"year"]) {
            //1 day = -31104000, - 21600 = -31125600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-31125600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                seTest = array[i];
                testdate9 = seTest.spotSilverTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"days"]) {
            //1 day = -86400, - 21600 = -108,000 seconds; 7 days = 626400
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-626400];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                seTest = array[i];
                testdate9 = seTest.spotSilverTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else { //preDateRange equals "24", also default value
            //1 day = -86400, - 21600 = -108,000 seconds; 7 days = 626400
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-108000];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                seTest = array[i];
                testdate9 = seTest.spotSilverTimeEntity;

                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
        }
    }
    if ([preGraphName isEqualToString:@"Platinum"]) {
        if ([preDateRange isEqualToString:@"oneMonth"]) {
            //1 day = -2592000, - 21600 = -2613600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-2613600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                teTest = array[i];
                testdate9 = teTest.spotPlatinumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"threeMonths"]) {
            //1 day = -7776000, - 21600 = -7797600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-7797600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                teTest = array[i];
                testdate9 = teTest.spotPlatinumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"sixMonths"]) {
            //1 day = -15552000, - 21600 = -15573600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-15573600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                teTest = array[i];
                testdate9 = teTest.spotPlatinumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"year"]) {
            //1 day = -31104000, - 21600 = -31125600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-31125600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                teTest = array[i];
                testdate9 = teTest.spotPlatinumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"days"]) {
            //1 day = -86400, - 21600 = -108,000 seconds; 7 days = 626400
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-626400];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                teTest = array[i];
                testdate9 = teTest.spotPlatinumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else { //preDateRange equals "24", also default value
            //1 day = -86400, - 21600 = -108,000 seconds; 7 days = 626400
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-108000];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                teTest = array[i];
                testdate9 = teTest.spotPlatinumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
        }

    }
    if ([preGraphName isEqualToString:@"Palladium"]) {
        if ([preDateRange isEqualToString:@"oneMonth"]) {
            //1 day = -2592000, - 21600 = -2613600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-2613600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                fteTest = array[i];
                testdate9 = fteTest.spotPalladiumTimeEntity;

                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"threeMonths"]) {
            //1 day = -7776000, - 21600 = -7797600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-7797600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                fteTest = array[i];
                testdate9 = fteTest.spotPalladiumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"sixMonths"]) {
            //1 day = -15552000, - 21600 = -15573600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-15573600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                fteTest = array[i];
                testdate9 = fteTest.spotPalladiumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"year"]) {
            //1 day = -31104000, - 21600 = -31125600 seconds
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-31125600];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                fteTest = array[i];
                testdate9 = fteTest.spotPalladiumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else if ([preDateRange isEqualToString:@"days"]) {
            //1 day = -86400, - 21600 = -108,000 seconds; 7 days = 626400
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-626400];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                fteTest = array[i];
                testdate9 = fteTest.spotPalladiumTimeEntity;
                
                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
            
        }
        else { //preDateRange equals "24", also default value
            //1 day = -86400, - 21600 = -108,000 seconds; 7 days = 626400
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-108000];
            NSDate *testdate9;
            
            for (int i = 0; i < array.count; i++) {
                fteTest = array[i];
                testdate9 = fteTest.spotPalladiumTimeEntity;

                if ([[date earlierDate:testdate9] isEqualToDate:date]){
                    numBarCount++;
                }
                
            }
        }
    }
    
    testDifference = array.count - numBarCount;
    //testDifference++;

    
}



- (NSUInteger)numberOfBarsInBarChartView:(JBBarChartView *)barChartView
{
    //return array.count;
    return numBarCount;
}


- (CGFloat)barChartView:(JBBarChartView *)barChartView heightForBarViewAtIndex:(NSUInteger)index
{
    double barHeight;
    
    if ([preGraphName isEqualToString:@"Gold"]) {
        
        fe = array[index + testDifference];
        NSString *test2 = fe.spotGoldEntity;
        
        if ([test2 hasPrefix:@"$"]) {
            test2 = [test2 substringFromIndex:1];
        }
        
        NSLog(test2);
        double testFloat = [test2 doubleValue];
        barHeight = testFloat;
        NSLog(@"%f", barHeight);
        
        NSNumber *testChart = [NSNumber numberWithFloat:barHeight];
        [chartData1 addObject:testChart];
      
    }
    
    if ([preGraphName isEqualToString:@"Silver"]) {
       
        se = array[index + testDifference];
        NSString *test2 = se.spotSilverEntity;
        
        if ([test2 hasPrefix:@"$"]) {
            test2 = [test2 substringFromIndex:1];
        }
        
        NSLog(test2);
        double testFloat = [test2 doubleValue];
        barHeight = testFloat;
        NSLog(@"%f", barHeight);
        
        NSNumber *testChart = [NSNumber numberWithFloat:barHeight];
        [chartData2 addObject:testChart];

    }
    
    if ([preGraphName isEqualToString:@"Platinum"]) {
        
        te = array[index + testDifference];
        NSString *test2 = te.spotPlatinumEntity;
        
        if ([test2 hasPrefix:@"$"]) {
            test2 = [test2 substringFromIndex:1];
        }
        
        NSLog(test2);
        double testFloat = [test2 doubleValue];
        barHeight = testFloat;
        NSLog(@"%f", barHeight);
        
        NSNumber *testChart = [NSNumber numberWithFloat:barHeight];
        [chartData3 addObject:testChart];
    }
    
    if ([preGraphName isEqualToString:@"Palladium"]) {
        
        fte = array[index + testDifference];
        NSString *test2 = fte.spotPalladiumEntity;
        
        if ([test2 hasPrefix:@"$"]) {
            test2 = [test2 substringFromIndex:1];
        }
        
        NSLog(test2);
        double testFloat = [test2 doubleValue];
        barHeight = testFloat;
        NSLog(@"%f", barHeight);
        
        NSNumber *testChart = [NSNumber numberWithFloat:barHeight];
        [chartData4 addObject:testChart];
    }
    
    
 
    return barHeight;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadHighLowAmounts {
    
    CGFloat largest = 0;
    CGFloat smallest = 100000;
    
    //////////////////////////
    
    if ([preGraphName isEqualToString:@"Gold"]) {
        for (int i = 0; i < chartData1.count; i++) {
            if ([chartData1[i] floatValue] > largest) {
                largest = [chartData1[i] floatValue];
            }
        }
        
        for (int i = 0; i < chartData1.count; i++) {
            if ([chartData1[i] floatValue] < smallest) {
                smallest = [chartData1[i] floatValue];
            }
        }
    }
    
    if ([preGraphName isEqualToString:@"Silver"]) {
        for (int i = 0; i < chartData2.count; i++) {
            if ([chartData2[i] floatValue] > largest) {
                largest = [chartData2[i] floatValue];
            }
        }
        
        for (int i = 0; i < chartData2.count; i++) {
            if ([chartData2[i] floatValue] < smallest) {
                smallest = [chartData2[i] floatValue];
            }
        }
    }
    
    if ([preGraphName isEqualToString:@"Platinum"]) {
        for (int i = 0; i < chartData3.count; i++) {
            if ([chartData3[i] floatValue] > largest) {
                largest = [chartData3[i] floatValue];
            }
        }
        
        for (int i = 0; i < chartData3.count; i++) {
            if ([chartData3[i] floatValue] < smallest) {
                smallest = [chartData3[i] floatValue];
            }
        }
    }
    
    if ([preGraphName isEqualToString:@"Palladium"]) {
        for (int i = 0; i < chartData4.count; i++) {
            if ([chartData4[i] floatValue] > largest) {
                largest = [chartData4[i] floatValue];
            }
        }
        
        for (int i = 0; i < chartData4.count; i++) {
            if ([chartData4[i] floatValue] < smallest) {
                smallest = [chartData4[i] floatValue];
            }
        }
    }
    
    
    _highAmount.text = [NSString stringWithFormat:@"%.2f", largest];
    _lowAmount.text = [NSString stringWithFormat:@"%.2f", smallest];
    

}



@end
