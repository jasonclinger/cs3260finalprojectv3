//
//  Tutorial.m
//  FinalProjectV2
//
//  Created by Jason Clinger on 4/2/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "Tutorial.h"

@implementation Tutorial

@synthesize title = _title;
@synthesize url = _url;

@end
