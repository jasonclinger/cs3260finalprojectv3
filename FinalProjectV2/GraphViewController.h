//
//  GraphViewController.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/31/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SecondEntity.h"
#import "ThirdEntity.h"
#import "FourthEntity.h"

@interface GraphViewController : UIViewController
{
    NSMutableArray *chartLegend;
    NSMutableArray *chartData1;
    NSMutableArray *chartData2;
    NSMutableArray *chartData3;
    NSMutableArray *chartData4;
    NSArray* array;
    NSArray* testArray;
    NSArray* dateArray;
    
    int numBarCount;
    int testDifference;
    
    @public NSString* preGraphName;
    NSString* preDateRange;
    NSString* preCurrentSpotPrice;
    
}

@property (strong, nonatomic) UIColor *barColor;
@property (strong, nonatomic) UIColor *backBarColor;


@property (nonatomic, readonly) BOOL reloading;


@property (weak, nonatomic) IBOutlet UILabel *highAmount;
@property (weak, nonatomic) IBOutlet UILabel *lowAmount;
@property (weak, nonatomic) IBOutlet UILabel *timePeriod;
@property (weak, nonatomic) IBOutlet UILabel *graphMetalTitle;
@property (weak, nonatomic) IBOutlet UILabel *currentSpotPriceGraph;






@end
