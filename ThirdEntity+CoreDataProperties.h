//
//  ThirdEntity+CoreDataProperties.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 4/11/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ThirdEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ThirdEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *spotPlatinumEntity;
@property (nullable, nonatomic, retain) NSDate *spotPlatinumTimeEntity;

@end

NS_ASSUME_NONNULL_END
