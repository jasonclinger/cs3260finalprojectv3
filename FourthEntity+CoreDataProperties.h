//
//  FourthEntity+CoreDataProperties.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 4/11/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FourthEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface FourthEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *spotPalladiumEntity;
@property (nullable, nonatomic, retain) NSDate *spotPalladiumTimeEntity;

@end

NS_ASSUME_NONNULL_END
