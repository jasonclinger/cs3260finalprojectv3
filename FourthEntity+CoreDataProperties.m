//
//  FourthEntity+CoreDataProperties.m
//  FinalProjectV2
//
//  Created by Jason Clinger on 4/11/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FourthEntity+CoreDataProperties.h"

@implementation FourthEntity (CoreDataProperties)

@dynamic spotPalladiumEntity;
@dynamic spotPalladiumTimeEntity;

@end
